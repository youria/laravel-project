@extends('layouts.admin')
@section('title', 'Person')
@section('content')
<div class="col-md-8">
         <div class"well">
    <div class="row">
                    <div class="navbar-header">
                <a href="" div class="navbar-brand">Person</a>
            </div>
            <div class="pull-right">
                <a href="{{action('PersonController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('PersonController@deletingOne', $person['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('PersonController@updatingOne', $person['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('PersonController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Voornaam:</strong>
                {{ $person->FirstName}}
            </div>
            <div class="form-group">
                <strong>Achternaam:</strong>
                {{ $person->LastName}}
            </div>
            <div class="form-group">
                <strong>E-mail:</strong>
                <a href="mailto:{{ $person->Email}}">{{ $person->Email}}</a>
            </div>
            <div class="form-group">
                <strong>Adres 1:</strong>
                {{ $person->Address1}}
            </div>
            <div class="form-group">
                <strong>Adres 2:</strong>
                {{ $person->Address2}}
            </div>
            <div class="form-group">
                <strong>Postcode:</strong>
                {{ $person->PostalCode}}
            </div>
            <div class="form-group">
                <strong>Stad:</strong>
                {{ $person->City}}
            </div>
            <div class="form-group">
                <strong>Land:</strong>
                {{ $person->getCountry->Name}}
            </div>
            <div class="form-group">
                <strong>Telefoon 1:</strong>
                {{$person->Phone1}}
            </div>
            <div class="form-group">
            <strong>Geboortedatum:</strong>
            @if($person->Birthday != null)
                {{$person->Birthday->format('Y-m-d')}}
                
            @else
                {{$person->Birthday}}
            @endif
            </div>
            <div class="form-group">
                  Tevreden: {{$person->Rating}} %
            </div>
        </div>
    </div>

</div>
</div>
@include('person.sidebar') 
@endsection