<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use App\Person;

class UserController extends Controller
{

    public function index()
    {
        
        return view('user/index')->with('users', $this->readingAll());
    }
    
    public function creatingOne()
    {
        $users = User::all();
        $roles = Role::pluck('Name', 'Id')->all();
        $persons = Person::pluck( 'FirstName',  'Id')->all();
        return view('user/creatingOne', compact('users', 'roles', 'persons'));
    }
    
    public function create(Request $request)
    {
        $this->validate($request, [
            'Name' => 'required',
            'HashedPassword' => 'required'
            ]);
            
            $user = User::create($request->all());
            $user->save();
        return view('user/index')->with('success', 'User added')->with('user', $user)->with('users', $this->readingAll());
    }
    
    public function readingAll()
    {
        $users = User::all();
        return $users;
    }
    
    public function readingOne($id)
    {
        
        $user = User::find($id);
        return view('user/readingOne')->with('user', $user)->with('users', $this->readingAll());
    }
    
    protected function getRole(){
        return Role::all();
    }
    protected function getPerson(){
        return Person::all();
    }

    public function updatingOne($id)
    {
      $user = User::find($id);
        return view('user/updatingOne')->with('user', $user)->with('users', $this->readingAll())->with('persons', $this->getPerson())->with('roles', $this->getRole());
    }
    
    public function update(Request $request, $id) 
    {
      $this->validate($request, [
            'Name' => 'required',
            'HashedPassword' => 'required'
            
            ]);
        
        $user = User::find($id);
        $user = $this->setUser($user, $request);

        $user->save();
        
        return redirect()->to('user/index')->send();
  
    }
    
    protected function setUser(User $user, $request){
        $user->Name = $request->input('Name');
        $user->Salt = $request->input('Salt');
        $user->HashedPassword = $request->input('HashedPassword');
        $user->PersonId = $request->input('PersonId');
        $user->RoleId = $request->input('RoleId');
        return $user;
    }

    public function deletingOne($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->to('user/index')->send();
    }
}