@extends('layouts.admin')
@section('title', 'User')
@section('content')

<div class="col-md-8">
    <div class="well">
        <div class="navbar nav-default">
            <div class="navbar-header">
                <a href="" div class="navbar-brand">User</a>
            </div>
            <div class="nav navbar-nav navbar-right" >
            <a class="btn btn-primary" href="{{ url('user/creatingOne') }}">Create</a>
            </div>
        </div>
    </div>
</div>
@include('user.sidebar')
@endsection