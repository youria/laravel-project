@extends('layouts.admin')
@section('title', 'Person')
@section('content')

<div class="col-md-8">
    <div class="well">
        <div class="navbar nav-default">
            <div class="navbar-header">
                <a href="" div class="navbar-brand">Person</a>
            </div>
            <div class="nav navbar-nav navbar-right" >
            <a class="btn btn-primary" href="{{ url('person/creatingOne') }}">Create</a>
            </div>
        </div>
    </div>
</div>
@include('person.sidebar')
@endsection