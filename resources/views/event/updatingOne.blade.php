@extends('layouts.admin')
@section('title', 'Event')
@section('content')
<div class="col-md-8">
         <div class="well">
<form method="post" enctype="multipart/form-data" action="{{action('EventController@update', ['id' => $event->Id])}}">
            <!--nodig wanneer je een post wilt doen-->
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="navbar-header">
                <a href="" class="navbar-brand">Event</a>
            </div>
                    <span style="float:right">
                        <button class="btn btn-primary">Update</button>
                        <a href="{{ action('EventController@index') }}"><span class="btn btn-primary">Cancel</span></a></span>
                
            </div>

            <div class="form-group row">
                <label for="Name" class="col-sm-2 col-form-label">Name: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Name" id="Name" value="{{$event->Name}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Loacation" class="col-sm-2 col-form-label">Location: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Location" id="Location" value="{{$event->Location}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Starts" class="col-sm-2 col-form-label">Starts: </label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="Starts" id="Starts" value="{{$event->Starts->format('Y-m-d')}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Ends" class="col-sm-2 col-form-label">Ends: </label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="Ends" id="Ends" value="{{$event->Ends->format('Y-m-d')}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Image" class="col-sm-2 col-form-label">Image: </label>
                <div class="col-sm-10">
                    <img src="/fricfrac//public/storage/cover_images/{{$event->Image}}">
                    <input type="file" name="Image" id="Image">
                </div>
            </div>

            <div class="form-group row">
                <label for="Description" class="col-sm-2 col-form-label">Description: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Description" id="Description" value="{{$event->Description}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="OrganiserName" class="col-sm-2 col-form-label">OrganiserName: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="OrganiserName" id="OrganiserName" value="{{$event->OrganiserName}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="OrganiserDescription" class="col-sm-2 col-form-label">OrganiserDescription: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="OrganiserDescription" id="OrganiserDescription" value="{{$event->OrganiserDescription}}">
                </div>
            </div>
            
            <div class="form-group row">
                <label for="EventCategoryId" class="col-sm-2 col-form-label">Event category: </label>
                <div class="col-sm-10">
                    <select class="form-control" name="EventCategoryId" id="EventCategoryId">
                        <option value=""></option>
                        @foreach($eventCategories AS $eventCategory)
                            @if($eventCategory->Id == $event->EventCategoryId)
                                <option value="{{$eventCategory->Id}}" selected>{{$eventCategory->Name}}</option>
                            @else
                                <option value="{{$eventCategory->Id}}">{{$eventCategory->Name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="EventTopicId" class="col-sm-2 col-form-label">Event topic: </label>
                <div class="col-sm-10">
                    <select class="form-control" name="EventTopicId" id="EventTopicId">
                        <option value=""></option>
                        @foreach($eventTopics AS $eventTopic)
                            @if($eventTopic->Id == $event->EventTopicId)
                                <option value="{{$eventTopic->Id}}" selected>{{$eventTopic->Name}}</option>
                            @else
                                <option value="{{$eventTopic->Id}}">{{$eventTopic->Name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            
</form>
</div>
</div>
@include('event.sidebar')
@endsection