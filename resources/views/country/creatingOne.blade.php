@extends('layouts.admin')
@section('title', 'Country')
@section('content')
<div class="col-md-8">
     <div class="well">
         <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Country</a>
            </div>
{!! Form::open(['url' => 'country/create']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    <a href="{{ url('country/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter land naam'])}}
</div>
<div class="form-group">
    {{Form::label('code', 'Code')}}
    {{Form::text('code', '', ['class' => 'form-control', 'placeholder' => 'Enter land code'])}}
</div>
{!! Form::close() !!}
    </div>
</div>
@include('country.sidebar')
@endsection