<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Country extends Model
{
    protected $table = "Country";
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $fillable=['Name','Code'];

}
