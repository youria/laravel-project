@extends('layouts.admin')
@section('title', 'Country')
@section('content')
 <div class="col-md-8">
          <div class="well">
        <form method="post" action="{{ url('country/update/'.$country->Id)}}">
            <!--nodig wanneer je een post wilt doen-->
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="navbar-header">
                <a href="" class="navbar-brand">Country</a>
            </div>
                <span style="float:right">
                    <button class="btn btn-primary">Update</button>
                    <a class="btn btn-primary"  href="{{ url('country/index')}}">Cancel</a>
                </span>
            </div>
            <div class="form-group row">
                <label for="Name" class="col-sm-2 col-form-label">Name: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Name" id="Name" value="{{$country->Name}}">
                </div>

                <label for="Code" class="col-sm-2 col-form-label">Code: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Code" id="Code" value="{{$country->Code}}">
                </div>
            </div>
        </form>
    </div>
    </div>
@include('country.sidebar')
@endsection