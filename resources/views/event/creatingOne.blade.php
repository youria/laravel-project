@extends('layouts.admin')
@section('title', 'Event')
@section('content')

    <div class="col-md-8">
         <div class="well">
             <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Event</a>
            </div>
{!! Form::open(['url' => 'event/create', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    {{ csrf_field() }}
    <a href="{{ url('event/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('Name', 'Naam')}}
    {{Form::text('Name', '', ['class' => 'form-control', 'placeholder' => 'Enter event name'])}}
</div>
<div class="form-group">
    {{Form::label('Location', 'Locatie')}}
    {{Form::text('Location', '', ['class' => 'form-control', 'placeholder' => 'Enter location name'])}}
</div>
<div class="form-group">
    {{Form::label('Starts', 'Start')}}
    {{Form::date('Starts', '', ['class' => 'form-control', 'placeholder' => 'Enter starts date'])}}
</div>
<div class="form-group">
    {{Form::label('Ends', 'Einde')}}
    {{Form::date('Ends', '', ['class' => 'form-control', 'placeholder' => 'Enter ends date'])}}
</div>
<div>
    {{Form::label('Image', 'Afbeelding')}}
    {{Form::file('Image')}}
</div>
<div class="form-group">
    {{Form::label('Description', 'Beschrijving')}}
    {{Form::text('Description', '', ['class' => 'form-control', 'placeholder' => 'Enter description'])}}
</div>
<div class="form-group">
    {{Form::label('Organisator name', 'Organisator naam')}}
    {{Form::text('OrganiserName', '', ['class' => 'form-control', 'placeholder' => 'Enter organiser name'])}}
</div>
<div class="form-group">
    {{Form::label('Organisator description', 'Organisator beschrijving')}}
    {{Form::text('OrganiserDescription', '', ['class' => 'form-control', 'placeholder' => 'Enter organisator description'])}}
</div>
<div class="form-group">
    {{Form::label('Event category', 'Event Organisator beschrijving')}}
    {{Form::select('EventCategoryId', [''=>'Kies de event category'] + $eventcategories ,null , ['class' => 'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('Event topic', 'Event topic')}}
    {{Form::select('EventTopicId', [''=>'Kies het event topic'] + $eventtopics, null, ['class' => 'form-control'])}}
</div>
{!! Form::close() !!}
</div>
</div>
@include('event.sidebar')
@endsection