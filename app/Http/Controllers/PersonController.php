<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Person;
use App\Country;


class PersonController extends Controller
{

    public function index()
    {
        $persons = Person::all();
        return view('person/index', compact('persons'));
    }
    
    public function creatingOne()
    {
        $persons = Person::all();
        $countries = Country::pluck('Name', 'Id')->all();
        return view('person/creatingOne', compact('persons', 'countries'));
    }
    
    public function create(Request $request)
    {
        $this->validate($request, [
            'FirstName' => 'required',
            'LastName' => 'required'
            ]);
            
            $person = Person::create($request->all());
            $person->save();
        return view('person/index')->with('success', 'Person added')->with('person', $person)->with('persons', $this->readingAll());
    }
    
    public function readingAll()
    {
        $persons = Person::all();
        return $persons;
    }
    
    public function readingOne($id)
    {
        
        $person = Person::find($id);
        return view('person/readingOne')->with('person', $person)->with('persons', $this->readingAll());
    }
    
    protected function getCountries(){
        return Country::all();
    }

    public function updatingOne($id)
    {
      $person = Person::find($id);
        return view('person/updatingOne')->with('person', $person)->with('persons', $this->readingAll())->with('countries', $this->getCountries());
    }
    
    public function update(Request $request, $id) 
    {
      $this->validate($request, [
            'FirstName' => 'required',
            'LastName' => 'required'
            
            ]);
        
        $person = Person::find($id);
        $person = $this->setPerson($person, $request);

        $person->save();
        
        return redirect()->to('person/index')->send();
  
    }
    
    protected function setPerson(Person $person, $request){
        $person->FirstName = $request->input('FirstName');
        $person->LastName = $request->input('LastName');
        $person->Email = $request->input('Email');
        $person->Address1 = $request->input('Address1');
        $person->Address2 = $request->input('Address2');
        $person->PostalCode = $request->input('PostalCode');
        $person->City = $request->input('City');
        $person->CountryId = $request->input('CountryId');
        $person->Phone1 = $request->input('Phone1');
        $person->Birthday = $request->input('Birthday');
        $person->Rating = $request->input('Rating');
        return $person;
    }

    public function deletingOne($id)
    {
        $person = Person::find($id);
        $person->delete();
        return redirect()->to('person/index')->send();
    }
}