<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventTopic;

class EventTopicController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $eventtopics = EventTopic::all()->toArray();
        
        return view('eventtopic/index', compact('eventtopics'));
    }
    
    
    public function creatingOne()
    {
        $eventtopics = EventTopic::all();
        
        return view('eventtopic/creatingOne')->with('eventtopics', $eventtopics);
    }
    
    
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'Required',
        ]); 
        
        //create event
        $eventtopic = new EventTopic;
        $eventtopic ->Name = $request->input('name');
        
        // save in database
        
        $eventtopic ->save();
        
        return redirect('eventtopic/index')->with('success', 'Event topic added');
    }
    
    
    public function readingOne($id)
    {
       $eventtopic = EventTopic::find($id);
        return view('eventtopic/readingOne')->with('eventtopic', $eventtopic)->with('eventtopics', $this->readingAll());

    }
    
    public function readingAll(){
        $eventtopics = EventTopic::all();
        return $eventtopics;
    }
    
    public function updatingOne($id)
    {
       $eventtopic = EventTopic::find($id);
        return view('eventtopic/updatingOne')->with('eventtopic', $eventtopic)->with('eventtopics', $this->readingAll());

    }
    
    public function update(Request $request, $id) 
    {
        $eventtopic = EventTopic::find($id);
        $eventtopic = $this->setEventCategory($eventtopic, $request);
        $eventtopic->save();
        return redirect()->to('eventtopic/index')->send();
    }
    
        protected function setEventCategory(EventTopic $eventtopic, $request){
        $eventtopic->Name = $request->input('Name');
        return $eventtopic;
    }
    
     public function deletingOne($id)
    {
        $eventtopic = EventTopic::find($id);
        $eventtopic->delete();
        return redirect()->action('EventTopicController@index');
    }
    
}