@extends('layouts.admin')
@section('title', 'Event topic')
@section('content')
<div class="col-md-8">
         <div class="well">
                          <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Event Topic</a>
            </div>
{!! Form::open(['url' => 'eventtopic/create']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    <a href="{{ url('eventtopic/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter topic naam'])}}
</div>
{!! Form::close() !!}
</div>
</div>
@include('eventtopic.sidebar')
@endsection