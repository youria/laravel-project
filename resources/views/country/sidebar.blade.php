<div class="col-md-4">
        <table class="table table-bordered">
             <tr>
                <th scope="col">Select</th>
                <th scope="col">Id</th>
                <th scope="col">Name</th>
                <th scope="col">Code</th>
            </tr>
            @foreach($countries as $country)
            <tr class="table table-hover">
                <td>
                    <a href="{{action('CountryController@readingOne', $country['Id'])}}">>>></a>
                </td>
                <td>
                    {{$country['Id']}}
                </td>
                <td>
                    {{$country['Name']}}
                </td>
                <td>
                    {{$country['Code']}}
                </td>
            </tr>
@endforeach
        </table>
</div>