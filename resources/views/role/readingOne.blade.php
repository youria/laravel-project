@extends('layouts.admin')
@section('title', 'Role')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Role</a>
            </div>

            <div class="pull-right">
                <a href="{{action('RoleController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('RoleController@deletingOne', $role['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('RoleController@updatingOne', $role['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('RoleController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $role->Name}}
            </div>
        </div>
    </div>
</div>
</div>
@include('role.sidebar')
@endsection