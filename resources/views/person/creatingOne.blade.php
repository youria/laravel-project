@extends('layouts.admin')
@section('title', 'Person')
@section('content')
<div class="col-md-8">
         <div class="well">
             <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Person</a>
            </div>
{!! Form::open(['url' => 'person/create', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    {{ csrf_field() }}
    <a href="{{ url('person/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('FirstName', 'Voornaam')}}
    {{Form::text('FirstName', '', ['class' => 'form-control', 'placeholder' => 'Enter voornaam'])}}
</div>
<div class="form-group">
    {{Form::label('LastName', 'Achernaam')}}
    {{Form::text('LastName', '', ['class' => 'form-control', 'placeholder' => 'Enter achternaam'])}}
</div>
<div class="form-group">
    {{Form::label('Email', 'E-mail')}}
    {{Form::text('Email', '', ['class' => 'form-control', 'placeholder' => 'Enter email'])}}
</div>
<div class="form-group">
    {{Form::label('Adress1', 'Adres1')}}
    {{Form::text('Address1', '', ['class' => 'form-control', 'placeholder' => 'Enter adres 1'])}}
</div>
<div class="form-group">
    {{Form::label('Adress2', 'Adres1')}}
    {{Form::text('Address2', '', ['class' => 'form-control', 'placeholder' => 'Enter adres 2'])}}
</div>
<div class="form-group">
    {{Form::label('PostalCode', 'Postcode')}}
    {{Form::text('PostalCode', '', ['class' => 'form-control', 'placeholder' => 'Enter postcode'])}}
</div>
<div class="form-group">
    {{Form::label('City', 'Stad')}}
    {{Form::text('City', '', ['class' => 'form-control', 'placeholder' => 'Enter stad'])}}
</div>
<div class="form-group">
    {{Form::label('CountryId', 'Land')}}
    {{Form::select('CountryId', [''=>'Kies de event category'] + $countries ,null , ['class' => 'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('Phone1', 'Telefoon 1')}}
    {{Form::text('Phone1', '', ['class' => 'form-control', 'placeholder' => 'Enter telefoon'])}}
</div>
<div class="form-group">
    {{Form::label('Birthday', 'Geboortedatum')}}
    {{Form::date('Birthday', '', ['class' => 'form-control', 'placeholder' => 'Enter geboortedatum'])}}
</div>
<div class="slidecontainer">
  <input type="range" min="1" max="100" value="50" class="slider" id="Rating" name="Rating">
  <p>Tevreden: <span id="RatingNumber"></span> %</p>
</div>
{!! Form::close() !!}
</div>
</div>
<script>
var slider = document.getElementById("Rating");
var output = document.getElementById("RatingNumber");
output.innerHTML = slider.value;

slider.oninput = function() {
  output.innerHTML = this.value;
}
</script>
@include('person.sidebar')
@endsection