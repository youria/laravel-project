<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\EventTopic;
use App\EventCategory;
use Illuminate\Support\Facades\Storage;


class EventController extends Controller

{

    public function index()
    {
        return view('event/index')->with('events', $this->readingAll());
    }
    
    
    public function creatingOne()
    {
        
        $eventtopics = EventTopic::pluck('Name', 'Id')->all();
        $eventcategories = EventCategory::pluck('Name', 'Id')->all();
        
        return view('event/creatingOne', compact('eventtopics','eventcategories'))->with('events', $this->readingAll());
    }
    
    protected function uploadImage($request){
        $filenameWithExtension = $request->file('Image')->getClientOriginalName();
        $filename = pathinfo($filenameWithExtension, PATHINFO_FILENAME);
        $extension = $request->file('Image')->getClientOriginalExtension();
        $filenameUnique = $filename."_".time().'.'.$extension;
        $request->file('Image')->storeAs('public/cover_images', $filenameUnique);
        return $filenameUnique;
    }
    
    
    public function create(Request $request)
    {
        $this->validate($request, [
            'Name' => 'required',
            
            ]);
            
        $event = new Event();
        $event = $this->setEvent($event, $request);
            
        if($request->file('Image')){
            $event->Image = $this->uploadImage($request);
        }else{
            $event->Image = "noimage.jpg";
        }
        
        $event->save();
        
        return view('event/index')->with('events', $this->readingAll());
    }
    

    public function readingOne($id)
    {
      $event = Event::find($id);
        return view('event/readingOne')->with('event', $event)->with('events', $this->readingAll());

    }
    
    public function readingAll()
    {
        $events = Event::all();
        return $events;
    }
    
    protected function getEventTopics(){
        return EventTopic::all();
    }

    protected function getEventCategories(){
        return EventCategory::all();
    }
    
     
    public function updatingOne($id)
    {
      $event = Event::find($id);
        return view('event/updatingOne')->with('event', $event)->with('events', $this->readingAll())->with('eventTopics', $this->getEventTopics())->with('eventCategories', $this->getEventCategories());;

    }
    
    public function update(Request $request, $id) 
    {
      $this->validate($request, [
            'Name' => 'required',
            'Location' => 'required',
            'Description' => 'required',
            'OrganiserDescription' => 'required',
            'OrganiserName' => 'required',
            ]);
        $event = Event::find($id);
        $event = $this->setEvent($event, $request);

        if($request->file('Image')){
            if($event->Image != 'noimage.jpg'){
                Storage::delete('public/cover_images/'.$event->Image);
            }

            $event->Image = $this->uploadImage($request);
        }
        $event->save();
        return redirect()->to('event/index')->send();
    }
    
    protected function setEvent(Event $event, $request)
    {
        $event->Name = $request->input('Name');
        $event->Location = $request->input('Location');
        $event->Starts = $request->input('Starts');
        $event->Ends = $request->input('Ends');
        $event->Description = $request->input('Description');
        $event->OrganiserName = $request->input('OrganiserName');
        $event->OrganiserDescription = $request->input('OrganiserDescription');
        $event->EventCategoryId = $request->input('EventCategoryId');
        $event->EventTopicId = $request->input('EventTopicId');
        return $event;
    }
    
    public function deletingOne($id)
    {
        $event = Event::find($id);
        Storage::delete('public/cover_images/'.$event->Image);
        $event->delete();
        return redirect()->to('event/index')->send();
    }
    
    public function eventsToPDFList()
    {
        return view('event/eventsList')->with('events', $this->readingAll());
    }
    
    public function eventsToPDF(Request $request){
        
        $events_selected = $request->input('eventSelection');
        $fpdf = new \Codedge\Fpdf\Fpdf\Fpdf();
        $fpdf->AddPage();
        $fpdf->Image('https://youriadriaensens-15241-climber32.c9users.io/fricfrac/public/storage/cover_images/fric-frac-logo.JPEG');
        $fpdf->SetFont('Arial','B',20);
        
        $events = Event::All();
        $fpdf->Cell('190','5','' ,0,2);
        if(isset($events_selected) === true)
        {
            foreach($events_selected as $event_selected)
            {
                $event = Event::find($event_selected);
 
                $fpdf->SetFont('Arial','B',20);
                $fpdf->Cell('190','15',$event->Name.'' , 1, 2);
                $fpdf->SetFont('Arial','B',10);
                $fpdf->Cell('190','15','Locatie:'.' '.$event->Location, 0, 2);
            }
        }else
        {
            $fpdf->Cell('190','50',"Geen events aangeduid", 1, 1,'C');
        }
        $fpdf->Output();
        exit;
    }
}