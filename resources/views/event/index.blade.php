@extends('layouts.admin')
@section('title', 'Event')
@section('content')

<div class="col-md-8">
    <div class="well">
        <div class="navbar nav-default">
            <div class="navbar-header">
                <a href="" div class="navbar-brand">Event</a>
            </div>
            <div class="nav navbar-nav navbar-right" >
                <a class="btn btn-primary" href="{{ url('event/eventstopdflist') }}"><img src="/fricfrac//public/storage/cover_images/pdf.png" width="50%" height="25%"></a>
                <a class="btn btn-primary" href="{{ url('event/creatingOne') }}">Create</a>
            </div>
        </div>
    </div>
</div>
@include('event.sidebar')
@endsection