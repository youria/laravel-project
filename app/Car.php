<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    protected $table = 'cars';
	protected $primaryKey = 'Id';
	public $timestamps = false;

	protected $fillable = [
		'make',
		'model'
	];
}
