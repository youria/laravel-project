<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventTopic extends Model
{
    protected $table = "EventTopic";
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $fillable=['Name'];
}
