<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $table = 'User';
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $fillable=['Name','Salt', 'HashedPassword', 'PersonId', 'RoleId'];

    public function getPerson(){
        return $this->belongsTo('App\Person', 'PersonId', 'Id');
    }
    public function getRole(){
        return $this->belongsTo('App\Role', 'RoleId', 'Id');
    }
}
