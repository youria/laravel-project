<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Country;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $countries = Country::all();
        
        return view('country/index', compact('countries'));
    }
    
    
    public function creatingOne()
    {
        $countries = Country::all();
        
        return view('country/creatingOne')->with('countries', $countries);
    }
    
    
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'Required',
        'code' => 'Required'
        ]); 
        
        //create event
        $country = new Country;
        $country ->Name = $request->input('name');
        $country ->Code = $request->input('code');
        
        // save in database
        
        $country ->save();
        
        return redirect('country/index')->with('success', 'Country added');
    }
    
    
    public function readingOne($id)
    {
       $country = Country::find($id);
        return view('country/readingOne')->with('country', $country)->with('countries', $this->readingAll());

    }
    
    public function readingAll(){
        $countries = Country::all();
        return $countries;
    }
    
    public function updatingOne($id)
    {
       $country = Country::find($id);
        return view('country/updatingOne')->with('country', $country)->with('countries', $this->readingAll());

    }
    
    public function update(Request $request, $id) 
    {
        $country = Country::find($id);
        $country = $this->setCountry($country, $request);
        $country->save();
        return redirect()->to('country/index')->send();
    }
    
    protected function setCountry(Country $country, $request){
        $country->Name = $request->input('Name');
        $country->Code = $request->input('Code');
        return $country;
    }
    
     public function deletingOne($id)
    {
        $country = Country::find($id);
        $country->delete();
        return redirect()->action('CountryController@index');
    }
    
}