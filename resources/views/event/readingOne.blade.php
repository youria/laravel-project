@extends('layouts.admin')
@section('title', 'Event')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
                     <div class="navbar-header">
                <a href="" class="navbar-brand">Event</a>
            </div>

            <div class="pull-right">
                <a href="{{action('EventController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('EventController@deletingOne', $event['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('EventController@updatingOne', $event['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('EventController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $event->Name}}
            </div>
            <div class="form-group">
                <strong>Locatie:</strong>
                {{ $event->Location}}
            </div>

            <div class="form-group">
                <strong>Event name:</strong>
                {{ $event->Starts->format('Y-m-d')}}
            </div>
            <div class="form-group">
                <strong>Einde:</strong>
                {{ $event->Ends->format('Y-m-d')}}
            </div>
            <div class="form-group">
                <strong>Afbeelding:</strong>
                <img style="width:50%" src="https://youriadriaensens-15241-climber32.c9users.io/fricfrac/public/storage/cover_images/{{$event->Image}}">
            </div>
            <div class="form-group">
                <strong>Beschrijving:</strong>
                {{ $event->Description}}
            </div>
            <div class="form-group">
                <strong>Organisator:</strong>
                {{ $event->OrganiserName}}
            </div>
            <div class="form-group">
                <strong>Organisator beschrijving:</strong>
                {{ $event->OrganiserDescription}}
            </div>
            <div class="form-group">
                <strong>Event category:</strong>
                {{$event->getEventCategory->Name}}
            </div>
            <strong>Event topic:</strong>
                {{$event->getEventTopic->Name}}
            </div>
        </div>
        
    </div>
</div>
</div>
@include('event.sidebar') 
@endsection