<nav class="navbar navbar-inverse">
    <div class="col-md-1 text-left">
        <a class="btn btn-primary" href="{{ url('home/index') }}">Home Index</a>
    </div>
    <div class="col-md-5 text-left">
        <a class="btn btn-primary" href="{{ url('admin/index') }}">Admin Index</a>
    </div>
    <div class="col-md-6 text-right">
        <a class="navbar-brand" href="{{ url('admin/index') }}">Fric-Frac</a>
    </div>
</nav>