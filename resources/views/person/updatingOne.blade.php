@extends('layouts.admin')
@section('title', 'Person')
@section('content')
<div class="col-md-8">
         <div class="well">
             
<form method="post" action="{{action('PersonController@update', ['id' => $person->Id])}}">
            <!--nodig wanneer je een post wilt doen-->
            {{ csrf_field() }}
            <div class="form-group row">
                            <div class="navbar-header">
                <a href="" class="navbar-brand">Person</a>
            </div>
                    <span style="float:right">
                        <button class="btn btn-primary">Update</button>
                        <a href="{{ action('PersonController@index') }}"><span class="btn btn-primary">Cancel</span></a></span>
                
            </div>

            <div class="form-group row">
                <label for="FirstName" class="col-sm-2 col-form-label">Voornaam: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="FirstName" id="FirstName" value="{{$person->FirstName}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="LastName" class="col-sm-2 col-form-label">Achernaam: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="LastName" id="LastName" value="{{$person->LastName}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Email" class="col-sm-2 col-form-label">E-mail: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Email" id="Email" value="{{$person->Email}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Address1" class="col-sm-2 col-form-label">Adres 1: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Address1" id="Address1" value="{{$person->Address1}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Address2" class="col-sm-2 col-form-label">Adres 2: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Address2" id="Address2" value="{{$person->Address2}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="PostalCode" class="col-sm-2 col-form-label">Postcode: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="PostalCode" id="PostalCode" value="{{$person->PostalCode}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="City" class="col-sm-2 col-form-label">Stad: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="City" id=City value="{{$person->City}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="CountryId" class="col-sm-2 col-form-label">Land: </label>
                <div class="col-sm-10">
                    <select class="form-control" name="CountryId" id="CountryId">
                        <option value=""></option>
                        @foreach($countries as $country)
                            @if($country->Id == $person->CountryId)
                                <option value="{{$country->Id}}" selected>{{$country->Name}}</option>
                            @else
                                <option value="{{$country->Id}}">{{$country->Name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

            <div class="form-group row">
                <label for="Phone1" class="col-sm-2 col-form-label">Telefoon: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Phone1" id=Phone1 value="{{$person->Phone1}}">
                </div>
            </div>  
            
            <div class="form-group row">
                <label for="Birthday" class="col-sm-2 col-form-label">Geboortedatum: </label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="Birthday" id=Birthday value="{{$person->Birthday->format('Y-m-d')}}">
                </div>
            </div> 
</form>
</div>
</div>
@include('person.sidebar')
@endsection

