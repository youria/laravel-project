@extends('layouts.admin')
@section('title', 'Beheer')
@section('content')
<section class="show-room index grid-icon">
    <a class="tile" href="{{ url('person/index') }}">
        <h1><span class="icon-user"></span></h1>
        <span class="screen-reader-text">Index Personen</span>
        <h1>Index Personen</h1>
    </a>
    <a class="tile" href="{{ url('country/index') }}">
        <span class="icon-earth"></span>
        <span class="screen-reader-text">Index Landen</span>
        <h1>Index Landen</h1>
    </a>
    <a class="tile" href="{{ url('role/index') }}">
        <span class="icon-user-tie"></span>
        <span class="screen-reader-text">Index Rollen</span>
        <h1>Index Rollen</h1>
    </a>
    <a class="tile" href="{{ url('user/index') }}">
        <span class="icon-users"></span>
        <span class="screen-reader-text">Index Gebruikers</span>
        <h1>Index Gebruikers</h1>
    </a>
    <a class="tile" href="{{ url('event/index') }}">
        <span class="icon-power"></span>
        <span class="screen-reader-text">Index Events</span>
        <h1>Index Events</h1>
    </a>
    <a class="tile" href="{{ url('eventcategory/index') }}">
        <span class="icon-price-tag"></span>
        <span class="screen-reader-text">Index Event Cat.</span>
        <h1>Index Event Cat.</h1>
    </a>
    <a class="tile" href="{{ url('eventtopic/index') }}">
        <span class="icon-price-tags"></span>
        <span class="screen-reader-text">Index Event Topics</span>
        <h1>Index Event Topics</h1>
    </a>
</section>
@endsection