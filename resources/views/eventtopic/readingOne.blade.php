@extends('layouts.admin')
@section('title', 'Event topic')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
         <div class="navbar-header">
                <a href="" class="navbar-brand">Event Topic</a>
            </div>

            <div class="pull-right">
                <a href="{{action('EventTopicController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('EventTopicController@deletingOne', $eventtopic['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('EventTopicController@updatingOne', $eventtopic['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('EventTopicController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $eventtopic->Name}}
            </div>
        </div>
        
    </div>
</div>
</div>
@include('eventtopic.sidebar')
@endsection