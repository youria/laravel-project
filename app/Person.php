<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Person extends Model
{
    protected $table = "Person";

    public $timestamps = false;

    protected $primaryKey = 'Id';
    protected $fillable=['FirstName','LastName', 'Email', 'Password', 'Address1', 'Address2', 'PostalCode','City','CountryId', 'Phone1', 'Birthday', 'Rating'];

    protected $dates=['Birthday'];

    public function getCountry(){
        return $this->belongsTo('App\Country', 'CountryId', 'Id');
    }
}
