@extends('layouts.admin')
@section('title', 'Role')
@section('content')
 <div class="col-md-8">
          <div class="well">
        <form method="post" action="{{ url('role/update/'.$role->Id)}}">
            <!--nodig wanneer je een post wilt doen-->
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="navbar-header">
                <a href="" class="navbar-brand">Role</a>
            </div>
                <span style="float:right">
                    <button class="btn btn-primary">Update</button>
                    <a class="btn btn-primary"  href="{{ url('role/index')}}">Cancel</a>
                </span>
            </div>
            <div class="form-group row">
                <label for="Name" class="col-sm-2 col-form-label">Name: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Name" id="Name" value="{{$role->Name}}">
                </div>
            </div>
        </form>
    </div>
    </div>
@include('role.sidebar')
@endsection