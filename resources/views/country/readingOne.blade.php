@extends('layouts.admin')
@section('title', 'Country')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Country</a>
            </div>

            <div class="pull-right">
                <a href="{{action('CountryController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('CountryController@deletingOne', $country['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('CountryController@updatingOne', $country['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('CountryController@index')}}" class="btn btn-primary">Cancel</a>

            
        </div>
    </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $country->Name}}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Code:</strong>
                {{ $country->Code}}
            </div>
        </div>
    </div>
    </div>
    </div>
@include('country.sidebar')
@endsection