@extends('layouts.admin')
@section('title', 'Country')
@section('content')

<div class="col-md-8">
    <div class="well">
        <div class="navbar nav-default">
            <div class="navbar-header">
                <a href="index" div class="navbar-brand">Country</a>
            </div>
            <div class="nav navbar-nav navbar-right" >
            <a class="btn btn-primary" href="{{ url('country/creatingOne') }}">Create</a>
            </div>
        </div>
    </div>
</div>
@include('country.sidebar')
@endsection