@extends('layouts.admin')
@section('title', 'User')
@section('content')
<div class="col-md-8">
         <div class="well">
<form method="post" action="{{action('UserController@update', ['id' => $user->Id])}}">
            <!--nodig wanneer je een post wilt doen-->
            {{ csrf_field() }}
            <div class="form-group row">
                <div class="navbar-header">
                <a href="" class="navbar-brand">User</a>
            </div>
                    <span style="float:right">
                        <button class="btn btn-primary">Update</button>
                        <a href="{{ action('UserController@index') }}"><span class="btn btn-primary">Cancel</span></a></span>
                
            </div>

            <div class="form-group row">
                <label for="Name" class="col-sm-2 col-form-label">Name: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Name" id="Name" value="{{$user->Name}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="Salt" class="col-sm-2 col-form-label">Zout: </label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="Salt" id="Salt" value="{{$user->Salt}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="HashedPassword" class="col-sm-2 col-form-label">Wachtwoord: </label>
                <div class="col-sm-10">
                    <input type="password" class="form-control" name="HashedPassword" id="HashedPassword" value="{{$user->HashedPassword}}">
                </div>
            </div>

            <div class="form-group row">
                <label for="PersonId" class="col-sm-2 col-form-label">Persoon: </label>
                <div class="col-sm-10">
                    <select class="form-control" name="PersonId" id="PersonId">
                        <option value=""></option>
                        @foreach($persons as $person)
                            @if($person->Id == $user->PersonId)
                                <option value="{{$person->Id}}" selected>{{$person->LastName}}</option>
                            @else
                                <option value="{{$person->Id}}">{{$person->LastName}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>
            
            <div class="form-group row">
                <label for="RoleId" class="col-sm-2 col-form-label">Rol: </label>
                <div class="col-sm-10">
                    <select class="form-control" name="RoleId" id="RoleId">
                        <option value=""></option>
                        @foreach($roles as $role)
                            @if($role->Id == $user->RoleId)
                                <option value="{{$role->Id}}" selected>{{$role->Name}}</option>
                            @else
                                <option value="{{$role->Id}}">{{$role->Name}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
            </div>

</form>
</div>
</div>
@include('user.sidebar')
@endsection