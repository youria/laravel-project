<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventCategory extends Model
{
    protected $table = "EventCategory";
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $fillable=['Name'];
}
