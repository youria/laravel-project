@extends('layouts.admin')
@section('title', 'Event category')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
                             <div class="navbar-header">
                <a href="" class="navbar-brand">Event Category</a>
            </div>

            <div class="pull-right">
                <a href="{{action('EventCategoryController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('EventCategoryController@deletingOne', $eventcategory['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('EventCategoryController@updatingOne', $eventcategory['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('EventCategoryController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $eventcategory->Name}}
            </div>
        </div>
        
    </div>
</div>
</div>
@include('eventcategory.sidebar')
@endsection