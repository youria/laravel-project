<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $roles = Role::all()->toArray();
        
        return view('role/index', compact('roles'));
    }
    
    
    public function creatingOne()
    {
        $roles = Role::all();
        
        return view('role/creatingOne')->with('roles', $roles);
    }
    
    
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'Required',
        ]); 
        
        //create event
        $role = new Role;
        $role ->Name = $request->input('name');
        
        // save in database
        
        $role ->save();
        
        return redirect('role/index')->with('success', 'Role added');
    }
    
    
    public function readingOne($id)
    {
       $role = Role::find($id);
        return view('role/readingOne')->with('role', $role)->with('roles', $this->readingAll());

    }
    
    public function readingAll(){
        $roles = Role::all();
        return $roles;
    }
    
    public function updatingOne($id)
    {
       $role = Role::find($id);
        return view('role/updatingOne')->with('role', $role)->with('roles', $this->readingAll());

    }
    
    public function update(Request $request, $id) 
    {
        $role = Role::find($id);
        $role = $this->setrole($role, $request);
        $role->save();
        return redirect()->to('role/index')->send();
    }
    
    protected function setrole(role $role, $request)
    {
        $role->Name = $request->input('Name');
        return $role;
    }
    
        public function deletingOne($id)
    {
        $role = Role::find($id);
        $role->delete();
        return redirect()->action('RoleController@index');
    }
}