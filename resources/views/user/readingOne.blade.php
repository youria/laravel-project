@extends('layouts.admin')
@section('title', 'User')
@section('content')
<div class="col-md-8">
         <div class="well">
    <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">User</a>
            </div>

            <div class="pull-right">
                <a href="{{action('UserController@creatingOne')}}" class="btn btn-primary">Create</a>
                <a href="{{action('UserController@deletingOne', $user['Id'])}}" class="btn btn-primary">Delete</a>
                <a href="{{action('UserController@updatingOne', $user['Id'])}}" class="btn btn-primary">Edit</a>
                <a href="{{action('UserController@index')}}" class="btn btn-primary">Cancel</a>
            </div>
            
        </div>
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Naam:</strong>
                {{ $user->Name}}
            </div>
            <div class="form-group">
                <strong>Zout:</strong>
                {{ $user->Salt}}
            </div>
            <div class="form-group">
                <strong>Person:</strong>
                {{ $user->getPerson->LastName}}
            </div>
            <div class="form-group">
                <strong>Rol:</strong>
                {{ $user->getRole->Name}}
            </div>

        </div>
    </div>
</div>
</div>
@include('user.sidebar') 
@endsection