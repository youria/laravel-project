@extends('layouts.admin')
@section('title', 'Role')
@section('content')
<div class="col-md-8">
         <div class="well">
             <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">Role</a>
            </div>
{!! Form::open(['url' => 'role/create']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    <a href="{{ url('role/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('name', 'Naam')}}
    {{Form::text('name', '', ['class' => 'form-control', 'placeholder' => 'Enter role name'])}}
</div>
{!! Form::close() !!}
</div>
</div>
@include('role.sidebar')
@endsection