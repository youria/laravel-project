<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <link rel="stylesheet" href="https://youriadriaensens-15241-climber32.c9users.io/fricfrac/public/css/app.css" type="text/css" />
     <!-- iconmoon iconen inladen -->
     <link rel="stylesheet" href="http://all-websolutions.be/laravel/style.css" type="text/css" />
     <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" >

    <title>Fric-Frac - @yield('title')</title>
</head>
<body>
    @include('inc.admin-navbar')
    <div class="container">
    <div class="row">
        <div class="col-8">
            @include('inc.messages')
            @yield('content')
        </div>
        
    </div>
     @include('inc.admin-footer')
    </div>
</body>
</html>