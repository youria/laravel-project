@extends('layouts.admin')
@section('title', 'Event')
@section('content')
<div class="col-md-8">
    <div class="well">
<form method="post" enctype="multipart/form-data" action="{{ action('EventController@eventsToPDF')}}" target="_BLANK">
        {{ csrf_field() }}
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Selectie</th>
                <th scope="col">Naam</th>
                <th scope='col'>Locatie</th>
                <th scope="col">Start</th>
                <th scope="col">Einde</th>
                <th scope="col">Afbeelding</th>
            </tr>
            </thead>
            <tbody>
            @foreach($events as $event)
                <tr>
                    <td><input type="checkbox" name="eventSelection[]" value="{{$event->Id}}"></td>
                    <td>{{$event->Name}}</td>
                    <td>{{$event->Location}}</td>
                    <td>{{$event->Starts->format('d-m-Y')}}</td>
                    <td>{{$event->Ends->format('d-m-Y')}}</td>
                    <td><img style="width:50%" src="https://youriadriaensens-15241-climber32.c9users.io/fricfrac/public/storage/cover_images/{{$event->Image}}"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
        <button class="btn btn-primary btn-lg">Download PDF</button>
</form>
</div>
</div>
@include('event.sidebar')
@endsection