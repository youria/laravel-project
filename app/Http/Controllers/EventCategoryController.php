<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EventCategory;

class EventCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $eventcategories = EventCategory::all()->toArray();
        
        return view('eventcategory/index', compact('eventcategories'));
    }
    
    
    public function creatingOne()
    {
        $eventcategories = EventCategory::all();
        
        return view('eventcategory/creatingOne')->with('eventcategories', $eventcategories);
    }
    
    
    public function create(Request $request)
    {
        $this->validate($request, [
        'name' => 'Required',
        ]); 
        
        //create event
        $eventcategory = new EventCategory;
        $eventcategory ->Name = $request->input('name');
        
        // save in database
        
        $eventcategory ->save();
        
        return redirect('eventcategory/index')->with('success', 'Event category added');
    }
    
    
    public function readingOne($id)
    {
       $eventcategory = EventCategory::find($id);
        return view('eventcategory/readingOne')->with('eventcategory', $eventcategory)->with('eventcategories', $this->readingAll());

    }
    
    public function readingAll(){
        $eventcategories = EventCategory::all();
        return $eventcategories;
    }
    
    public function updatingOne($id)
    {
       $eventcategory = EventCategory::find($id);
        return view('eventcategory/updatingOne')->with('eventcategory', $eventcategory)->with('eventcategories', $this->readingAll());

    }
    
    public function update(Request $request, $id) 
    {
        $eventcategory = EventCategory::find($id);
        $eventcategory = $this->setEventCategory($eventcategory, $request);
        $eventcategory->save();
        return redirect()->to('eventcategory/index')->send();
    }
    
        protected function setEventCategory(EventCategory $eventcategory, $request){
        $eventcategory->Name = $request->input('Name');
        return $eventcategory;
    }
    
     public function deletingOne($id)
    {
        $eventcategory = EventCategory::find($id);
        $eventcategory->delete();
        return redirect()->action('EventCategoryController@index');
    }
    
}