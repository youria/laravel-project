@extends('layouts.admin')
@section('title', 'User')
@section('content')
<div class="col-md-8">
         <div class="well">
                          <div class="row">
             <div class="navbar-header">
                <a href="" class="navbar-brand">User</a>
            </div>
{!! Form::open(['url' => 'user/create', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}
<div class="text-right"> 
    {{Form::submit('Insert', ['class' => 'btn btn-primary'])}}
    {{ csrf_field() }}
    <a href="{{ url('user/index') }}" class="btn btn-primary">Cancel</a>
</div>
</div>
<div class="form-group">
    {{Form::label('Name', 'Naam')}}
    {{Form::text('Name', '', ['class' => 'form-control', 'placeholder' => 'Enter naam'])}}
</div>
<div class="form-group">
    {{Form::label('Salt', 'Zout')}}
    {{Form::text('Salt', '', ['class' => 'form-control', 'placeholder' => 'Enter zout'])}}
</div>
<div class="form-group">
    {{Form::label('HashedPassword', 'Wachtwoord')}}
    {{Form::password('HashedPassword',  ['class' => 'form-control', 'placeholder' => 'Enter wachtwoord'] )}}
</div>
<div class="form-group">
    {{Form::label('PersonId', 'Persoon')}}
    {{Form::select('PersonId', [''=>'Kies een persoon'] + $persons ,null , ['class' => 'form-control'])}}
</div>
<div class="form-group">
    {{Form::label('RoleId', 'Rol')}}
    {{Form::select('RoleId', [''=>'Kies een rol'] + $roles ,null , ['class' => 'form-control'])}}
</div>
{!! Form::close() !!}
</div>
</div>
@include('user.sidebar')
@endsection




