<?php

//Home
Route::get('/', 'HomeController@index');
Route::get('home/index', 'HomeController@index');
Route::get('admin/index', 'HomeController@beheer');

//Admin/Index
Route::get('admin/index', 'AdminController@index');

//Country
Route::get('country/index', 'CountryController@index');
Route::get('country/creatingOne', 'CountryController@creatingOne');
Route::post('country/create', 'CountryController@create');
Route::get('country/readingOne/{id}', 'CountryController@readingOne');
Route::get('country/updatingOne/{id}', 'CountryController@updatingOne');
Route::post('country/update/{id}', 'CountryController@update');
Route::get('country/deletingOne/{id}', 'CountryController@deletingOne');

//Role
Route::get('role/index', 'RoleController@index');
Route::get('role/creatingOne', 'RoleController@creatingOne');
Route::post('role/create', 'RoleController@create');
Route::get('role/readingOne/{id}', 'RoleController@readingOne');
Route::get('role/updatingOne/{id}', 'RoleController@updatingOne');
Route::post('role/update/{id}', 'RoleController@update');
Route::get('role/deletingOne/{id}', 'RoleController@deletingOne');

//Event category
Route::get('eventcategory/index', 'EventCategoryController@index');
Route::get('eventcategory/creatingOne', 'EventCategoryController@creatingOne');
Route::post('eventcategory/create', 'EventCategoryController@create');
Route::get('eventcategory/readingOne/{id}', 'EventCategoryController@readingOne');
Route::get('eventcategory/updatingOne/{id}', 'EventCategoryController@updatingOne');
Route::post('eventcategory/update/{id}', 'EventCategoryController@update');
Route::get('eventcategory/deletingOne/{id}', 'EventCategoryController@deletingOne');

//Event topic
Route::get('eventtopic/index', 'EventTopicController@index');
Route::get('eventtopic/creatingOne', 'EventTopicController@creatingOne');
Route::post('eventtopic/create', 'EventTopicController@create');
Route::get('eventtopic/readingOne/{id}', 'EventTopicController@readingOne');
Route::get('eventtopic/updatingOne/{id}', 'EventTopicController@updatingOne');
Route::post('eventtopic/update/{id}', 'EventTopicController@update');
Route::get('eventtopic/deletingOne/{id}', 'EventTopicController@deletingOne');

//Event
Route::get('event/index', 'EventController@index');
Route::get('event/creatingOne', 'EventController@creatingOne');
Route::post('event/create', 'EventController@create');
Route::get('event/readingOne/{id}', 'EventController@readingOne');
Route::get('event/updatingOne/{id}', 'EventController@updatingOne');
Route::post('event/update/{id}', 'EventController@update');
Route::get('event/deletingOne/{id}', 'EventController@deletingOne');

// Events to PDF
Route::get('event/eventstopdflist', 'EventController@eventsToPDFList');
Route::post('event/eventstopdf', 'EventController@eventsToPDF');

//Person
Route::get('person/index', 'PersonController@index');
Route::get('person/creatingOne', 'PersonController@creatingOne');
Route::post('person/create', 'PersonController@create');
Route::get('person/readingOne/{id}', 'PersonController@readingOne');
Route::get('person/updatingOne/{id}', 'PersonController@updatingOne');
Route::post('person/update/{id}', 'PersonController@update');
Route::get('person/deletingOne/{id}', 'PersonController@deletingOne');

//User
Route::get('user/index', 'UserController@index');
Route::get('user/creatingOne', 'UserController@creatingOne');
Route::post('user/create', 'UserController@create');
Route::get('user/readingOne/{id}', 'UserController@readingOne');
Route::get('user/updatingOne/{id}', 'UserController@updatingOne');
Route::post('user/update/{id}', 'UserController@update');
Route::get('user/deletingOne/{id}', 'UserController@deletingOne');