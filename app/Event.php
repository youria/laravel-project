<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'Event';
    public $timestamps = false;
    protected $primaryKey = 'Id';
    protected $fillable=['Name','Location', 'Starts', 'Ends', 'Image','Description', 'OrganiserName', 'OrganiserDescription', 'EventCategoryId', 'EventTopicId'];
    protected $dates = ['Starts','Ends'];

    public function getEventCategory(){
        return $this->belongsTo('App\EventCategory', 'EventCategoryId', 'Id');
    }
    public function getEventTopic(){
        return $this->belongsTo('App\EventTopic', 'EventTopicId', 'Id');
    }


}
